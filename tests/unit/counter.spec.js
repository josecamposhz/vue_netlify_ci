import { mount } from '@vue/test-utils'
import Counter from '@/components/Counter.vue'

test('increments counter value on click', async () => {
    const wrapper = mount(Counter)
    const upButton = wrapper.find('#up')
    const downButton = wrapper.find('#down')
    const text = wrapper.find('p')

    expect(text.text()).toContain('Total clicks: 0')

    await upButton.trigger('click')

    expect(text.text()).toContain('Total clicks: 1')

    await downButton.trigger('click')

    expect(text.text()).toContain('Total clicks: 1')
})